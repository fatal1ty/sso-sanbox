const config = {
    idp: {
        response_type: 'token id_token',
        client_id: 'client',
        redirect_uri: 'http://localhost:3000/callback',
        scope: 'openid',
        nonce: 5,
        state: 3
    },
    openid: {
        auth_url: 'https://bell.local:9002/authorizationserver/oauth/authorize',
        token_url: 'https://bell.local:9002/authorizationserver/oauth/token',
        user_info_endpoint: 'https://bell.local:9002/bellcommercewebservices/v2/bell/openid/userinfo',
        client_id: 'client',
        client_secret: 'secret',
        scopes: 'openid',
        callback_url: 'http://localhost:3000/openidcallback'
    }
};

module.exports = config;