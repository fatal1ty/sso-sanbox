var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var config = require('./config');

var index = require('./routes/index');
var users = require('./routes/users');
var appendQuery = require('append-query');
var axios = require('axios');
var querystring = require('querystring');
var jwt = require('jsonwebtoken');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'hbs');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));


app.use('/', index);
app.use('/users', users);

app.get('/auth', function (req, res, done) {

  var authURL = appendQuery('https://localhost:9002/authorizationserver/oauth/authorize', config.idp);
  console.log("Auth URL: " + authURL);
  res.redirect(authURL);
});

app.get('/callback', function (req, res, done) {
  res.status(200).json(req.query);
});

process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";

/**
 * Open ID connect
 */

app.get('/openid', function (req, res, done) {

    var authUrl = appendQuery(config.openid.auth_url, {
        response_type: 'code',
        client_id: config.openid.client_id,
        redirect_uri: config.openid.callback_url,
        scope: config.openid.scopes
    });

    console.log("Auth URL: " + authUrl);
    res.redirect(authUrl);
});

app.get('/openidcallback', function (req, res, done) {

    var authCode = req.query.code;

    console.log("Auth code: " + authCode);

    var params = {
        code: authCode,
        client_id: config.openid.client_id,
        client_secret: config.openid.client_secret,
        redirect_uri: config.openid.callback_url,
        grant_type: 'authorization_code',
        response_type: 'token id_token'
    };

    // Exchange the auth code for the token
    axios.post(config.openid.token_url, querystring.stringify(params), {
        }).then(function (response) {

            var email = "";
            console.log("server responded! \n");
            console.log(response);
            //res.status(200).json(response.data);
            if (response.data && response.data.id_token) {

                var tokenData = response.data;
                var decodedToken = jwt.decode(response.data.id_token, {complete: true});
                email = decodedToken.payload.sub;
                var accessToken = response.data.access_token;

                console.log(" \n\n Email: " + email);
                console.log(" \n   Access Token: " + accessToken);

                // Exchange the token for the user profile information (access_token is used to identity the user)
                axios.get(config.openid.user_info_endpoint, {
                    headers: {
                        'Authorization': 'Bearer ' + accessToken
                    }
                }).then(function (response) {

                    // Print out the response sent by the user info endpoint
                    console.log("\n User Profile: \n");
                    console.log(response.data);

                    res.render('result', {
                        tokenData: JSON.stringify(tokenData, null, 4),
                        userProfile: JSON.stringify(response.data, null, 4),
                        decodedToken: {
                            header: JSON.stringify(decodedToken.header, null, 4),
                            payload: JSON.stringify(decodedToken.payload, null, 4)
                        },
                        email: email
                    });

                }).catch(function (err) {
                    console.log("Failed to send request due to: \n");
                    console.log(err.data);
                });
            }
            else {
                res.status(500).json({error: "Unable to render page!!"});
            }
        }).catch(function (err) {

            console.log("Failed to send request due to: \n");
            console.log(err);
            res.send(err.data);
        });
});

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
